#!/usr/bin/guile \
-e main -s
!#

;;; Tokenvote
;;; Copyright (C) 2015 Christopher Allan Webber <cwebber@dustycloud.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (web server)
             (web request)
             (web response)
             (web uri)
             (web http)
             (ice-9 match)
             (ice-9 regex)
             (ice-9 format)
             (srfi srfi-1)
             (srfi srfi-9)
             (rnrs bytevectors)
             (oop goops)
             (sxml simple)
             (gdbm)
             (ice-9 pretty-print)
             ((system repl server)
              #:prefix repl-server:))

(define *unique-id-random-state* (make-parameter (random-state-from-platform)))

(define (unique-id)
  "Generate and return a unique id"
  ;; Not really a formal method, but a larger number than a uuid4
  (format #f "~x" (random (expt 10 50) (*unique-id-random-state*))))


(define (list-of-tokens how-many)
  "Generate a list of HOW-MANY unique tokens

Useful for testing!"
  (map
   (lambda _
     (unique-id))
   (iota how-many)))


;; Question types:
;;  - select-one     <- interfaces: radio, dropdown
;;  - select-multiple
;;  - checkbox
;;  - text-box

;;; -------------------------
;;; Question and answer stuff
;;; -------------------------

(define (mandatory-keyword keyword)
  "GOOPS helper for making certain init keywords mandatory"
  (lambda ()
    (throw 'missing-init-keyword keyword)))

(define-class <question> ()
  ;; Primary name for this question
  (name #:init-keyword #:name
        #:init-thunk (mandatory-keyword #:name)
        #:getter get-name)
  ;; Detailed description of the question for the user
  (detail #:init-keyword #:detail
          #:init-value #f
          #:getter get-detail)
  ;; identifier
  (id #:init-keyword #:id
      #:init-thunk unique-id
      #:getter get-id
      #:setter set-id!))

;; This is the method for rendering the whole question, including
(define-generic question-render-html)
(define-generic question-render-field)
(define-method (question-render-html (question <question>) answer)
  `(div (@ (class "tokenvote-question"))
        (p (@ (class "question-name"))
           (b ,(get-name question))
           ,@(if (eq? (get-detail question) #f)
                 `((br) (i ,(get-detail question)))
                 '()))
        (div (@ (class "question-field"))
             ,(question-render-field question answer))))

(define-method (question-render-html (question <question>))
  (question-render-html question #f))


(define (answer-take-one question answer)
  "From a question/answer pair, take one from ANSWER"
  (match answer
    ;; Answers come back in lists.  Here we're assuming
    ;; this is for a single-answer type response
    ((? pair? r)
     (car answer))
    ;; #f usually means no answer supplied.
    (#f #f)
    ;; Not sure how we'd get an empty list, but okay, #f
    (() #f)
    (_
     (throw 'invalid-answer-type question answer))))

(define (answer-take-many question answer)
  "From a question/answer pair, take one from ANSWER"
  (match answer
    ;; Answers come back in lists.  Here we're assuming
    ;; this is for a multi-answer type response
    (() '())
    ((? pair? r)
     r)
    ;; #f usually means no answer supplied,
    ;; so in this case return an empty list
    (#f '())
    (_
     (throw 'invalid-answer-type question answer))))

(define-method (question-process-answer (question <question>) answer)
  "Process/validate an ANSWER for QUESTION for storage / representation

By default we just return the car of the answer, or #f :)"
  (answer-take-one question answer))

;; This might be subclassed per application

(define-class <request-context> ()
  (request #:init-keyword #:request
           #:init-thunk (mandatory-keyword #:request)
           #:getter ctx-request)
  (body #:init-keyword #:body
        #:init-thunk (mandatory-keyword #:body)
        #:getter ctx-body))

(define-generic ctx-method)
(define-method (ctx-method (ctx <request-context>))
  (request-method (ctx-request ctx)))

(define (parse-formdata formdata)
  "Take a formdata string (or bytevector) and parse into vhash"
  (let ((formdata
         (if (bytevector? formdata)
             (utf8->string formdata)
             formdata))
        (hash-table (make-hash-table)))
    (begin
      (for-each
       (lambda (x)
         (match (string-split x (lambda (char) (char=? #\= char)))
           ((key val)
            (hash-set! hash-table key
                       (cons val
                             (hash-ref hash-table key '()))))))
       (string-split formdata
                     (lambda (x) (char=? #\& x))))
      hash-table)))

(define-generic ctx-formdata)
(define-method (ctx-formdata (ctx <request-context>))
  "Convenient shortcut for (parse-formdata (ctx-body ctx))"
  (parse-formdata (ctx-body ctx)))

;; These are sadly all pretty similar in their methods

(define-class <select-one> (<question>)
  (options #:init-keyword #:options
           #:getter get-options))
(define-class <select-one-radio> (<select-one>))
(define-class <select-multiple> (<select-one>))

(define-method (question-process-answer (question <select-multiple>) answer)
  (answer-take-many question answer))

(define-method (question-render-field (question <select-one>) answer)
  (define (render-option value body is-selected)
    `(option
      (@ (value ,value)
         ,@(if is-selected
               '((selected "true"))
               '()))
      ,body))
  `(select
    ;; "name" in the form sense :)
    (@ (name ,(get-id question)))
    ,@(map
       (lambda (option)
         (match option
           ((identifier text default)
            (render-option identifier text default))
           ((identifier text)
            (render-option identifier text #f))))
       (get-options question))))

(define-method (question-render-field (question <select-one-radio>) answer)
  (define (render-option value body is-selected)
    `(li (input
          (@ (type "radio")
             (value ,value)
             ;; "name" in the form sense :)
             (name ,(get-id question))
             ,@(if is-selected
                   '((checked "true"))
                   '()))
          ,body)))
  `(ul
    ,@(map
       (lambda (option)
         (match option
           ((identifier text default)
            (render-option identifier text default))
           ((identifier text)
            (render-option identifier text #f))))
       (get-options question))))

(define-method (question-render-field (question <select-multiple>) answer)
  (define (render-option value body is-selected)
    `(li (input
          (@ (type "checkbox")
             (value ,value)
             ;; "name" in the form sense :)
             (name ,(get-id question))
             ,@(if is-selected
                   '((checked "true"))
                   '()))
          ,body)))
  `(ul
    ,@(map
       (lambda (option)
         (match option
           ((identifier text default)
            (render-option identifier text default))
           ((identifier text)
            (render-option identifier text #f))))
       (get-options question))))

(define-class <checkbox> (<question>)
  (check-text #:init-value ""
              #:init-keyword #:check-text
              #:getter check-text))

(define-method (question-render-field (question <checkbox>) answer)
  `(div
    (span (@ (class "check-text"))
          ,(check-text question))
    (input (@ (type "checkbox")
              (name ,(get-id question))
              ,@(if answer
                    '((checked "true"))
                    #nil)))))

(define-class <text-oneline> (<question>))
(define-class <text-multiline> (<question>))

(define-method (question-render-field (question <text-oneline>) answer)
  `(input (@ (type "text")
             (name ,(get-id question))
             ,@(if answer
                   '((value ,answer))
                   #nil))))

(define-method (question-render-field (question <text-multiline>) answer)
  `(textarea (@ (name ,(get-id question)))
             ,(if answer
                  (list answer)
                  "")))

(define questions
  (list
   (make <select-one>
     #:id "color-one"
     #:name "What's your favorite color?"
     #:options '(("blue" "I like blue")
                 ("red" "How about red?" #t)
                 ("purple" "Let's do purple")))
   (make <select-one-radio>
     #:id "color-radio"
     #:name "What's your favorite color?"
     #:options '(("blue" "I like blue")
                 ("red" "How about red?" #t)
                 ("purple" "Let's do purple")))
   (make <select-multiple>
     #:id "feelings"
     #:name "How are you feeling today?"
     #:options '(("happy" "Reasonably happy")
                 ("sad" "Sad :(")
                 ("anxious" "Anxious...")))
   (make <checkbox>
     #:id "satisfaction"
     #:name "Are you satisfied with this quiz?"
     #:check-text "Yes")
   (make <text-multiline>
     #:id "wish"
     #:name "If you could wish for one thing, what would it be?")
   (make <text-oneline>
     #:id "serve-better"
     #:name "Let us know how we could serve you better!")))

(define (build-response-statistics db)
  "Build up a hash table of all responses"
  (let ((all-responses (make-hash-table)))
    (gdbm-for-each
     (lambda (key val)
       (let ((parsed-responses (call-with-input-string val (lambda (p) (read p)))))
         (if parsed-responses
             (for-each
              (lambda (response)
                (let* ((question-id (assoc-ref response 'question-id))
                       (answer (assoc-ref response 'answer))
                       (question-responses
                        (or (hash-ref all-responses question-id)
                            (make-hash-table))))
                  ;; increment the number of times this question got this answer
                  ;; by one
                  (hash-set! question-responses answer
                             (1+ (hash-ref question-responses answer 0)))
                  (hash-set! all-responses question-id
                             question-responses)))
              parsed-responses))))
     db)
    all-responses))

;; This is just for my own pretty-printing convenience
(define (response-table->alist response-table)
  (hash-map->list
   (lambda (key val)
     (cons key (hash-map->list cons val)))
   response-table))

;; Homepage, directs user to

(define test-value #f)
(define (index ctx)
  (values (build-response)
          "noooooooooooooooesy"))

(define (not-found ctx)
  (values (build-response #:code 404)
          (string-append "Resource not found: "
                         (uri->string (request-uri (ctx-request ctx))))))


(define (templatize title body)
  (define (maybe-wrap-body)
    (if (and (pair? body)
             (pair? (car body)))
        body
        (list body)))
  `(html (head (title ,title))
         (body ,@(maybe-wrap-body))))

(define* (respond #:optional body #:key
                  (status 200)
                  (title "Hello hello!")
                  (doctype "<!DOCTYPE html>\n")
                  (content-type-params '((charset . "utf-8")))
                  (content-type 'text/html)
                  (extra-headers '())
                  (sxml (and body (templatize title body))))
  (values (build-response
           #:code status
           #:headers `((content-type
                        . (,content-type ,@content-type-params))
                       ,@extra-headers))
          (lambda (port)
            (if sxml
                (begin
                  (if doctype (display doctype port))
                  (sxml->xml sxml port))))))


(define (get-answers-from-formdata formdata questions)
  "Take FORMDATA and QUESTIONS and extract an alist of responses"
  (map (lambda (question)
         `((question-id . ,(get-id question))
           (question-name . ,(get-name question))
           ;; we need some sort of smart answer parser here
           ;; to be bundled with the question types
           (answer . ,(question-process-answer
                       question
                       (hash-ref
                        formdata
                        (get-id question))))))
       questions))


(define (save-answers app token answers)
  "Save answers for TOKEN in APP's db, assuming TOKEN is valid"
  ;; TODO: Check for token here :)
  (gdbm-set! (get-db app) token
             (with-output-to-string
               (lambda ()
                 (write answers)))))


(define (render-result-table questions question-id answers)
  (let ((question (find (lambda (q) (equal? (get-id q) question-id))
                        questions))
        (sorted-answers
         (sort (hash-map->list cons answers)
               (lambda (x) (char-ci<? (car x))))))
    (define (answer-as-row answer)
      (match answer
        ((text . num-responses)
         `(tr (td ,text)
              (td ,(number->string num-responses))))))
    `(div
      (p (b ,(get-name question)))
      (table
       ;; header
       (tr (th "Answer")
           (th "# of responses"))
       ,@(map answer-as-row sorted-answers)))))

(define (results-template results questions)
  `((h1 "Survey results")
    ,@(hash-map->list (lambda (key val)
                        (render-result-table questions key val))
                      results)))

(define (results-view ctx)
  (let ((statistics
         (build-response-statistics (get-db (app))))
        (questions (get-questions (app))))
    (respond (results-template statistics questions)
             #:title "Survey Responses")))

(define (quiz-template questions)
  `((form
     (@ (action "/quiz/")
        (method "post"))
     ,(map question-render-html
           questions)
     (input (@ (type "submit"))))))

(define (quiz-view ctx)
  (set! test-value ctx)
  (match (ctx-method ctx)
    ('GET
     (respond (quiz-template questions)
              #:title "Hello quiz!"))
    ('POST
     (let ((answers
            (get-answers-from-formdata
             (ctx-formdata ctx)
             (get-questions (app))))
           ;; TODO: Have a real token as part of this, this
           ;;   is just for testing for now :)
           (token (unique-id)))
       (save-answers (app) token answers)
       (respond
        `(div
          (p
           "Thanks for submitting!  Here are your answers in lispy form:")
          (pre
           ,(with-output-to-string
              (lambda () (pretty-print answers))))))))))

(define (dispatch-handler request)
  "Find the appropriate handler for this request"
  (match (uri-path (request-uri request))
    ("/"
     index)
    ("/quiz/"
     quiz-view)
    ("/results/"
     results-view)
    ;; regexp matching test
    ((= (lambda (x) (string-match "^/foo" x))
        (? regexp-match? re-match))
     (lambda _
       (values (build-response)
               "whee")))
    (_
     not-found)))

;; ---------------------
;; Application structure
;; ---------------------

(define-class <tokenvote-app> ()
  (questions #:init-keyword #:questions
             #:init-thunk (mandatory-keyword #:questions)
             #:getter get-questions)
  (db #:init-keyword #:db
      #:init-thunk (mandatory-keyword #:db)
      #:getter get-db)
  (tokens #:init-keyword #:tokens
          #:init-thunk (mandatory-keyword #:tokens)
          #:getter get-tokens))

(define app (make-parameter #f))

(define-method (app-close-db (app <tokenvote-app>))
  (gdbm-close (get-db app)))

;; ======================================================
;; redefining below stuff doesn't affect the "live REPL",
;; so if you change stuff here restart the process
;; ======================================================

(define (request-handler request body)
  (match (dispatch-handler request)
    ((? procedure? handler)
     (handler (make <request-context>
                #:request request
                #:body body)))
    (_
     (throw 'handler-not-a-procedure))))

;; TODO: Actually print the stack, too
(define (wrap-web-debugged proc)
  "Wrap output in a little web debug window"
  (lambda (request body)
    (catch
      #t
      (lambda ()
        (proc request body))
      (lambda (key . args)
        (respond
         `(div
           (h1 "Uhoh, something broke!")
           (p "Something went wrong.  Here's the exception:")
           (p
            (h2 "Throw to key: " (i ,(symbol->string key)))
            (pre
             ,(with-output-to-string
                (lambda ()
                  (pretty-print args))))))
         #:title "Danger, Will Robinson!"))
      (lambda _
        (backtrace)))))

(define default-handler request-handler)
;; (define default-handler (wrap-web-debugged request-handler))
(define (start-web-server)
  (run-server
   ;; abstracted one step away so the handler can be easily redefined
   (lambda args
     (apply default-handler args))))

(define (main args)
  (app
   (make <tokenvote-app>
     #:questions questions
     #:db (gdbm-open "/tmp/poll.db" GDBM_WRCREAT)
     #:tokens (list-of-tokens 20)))
  (repl-server:spawn-server)
  (start-web-server))
